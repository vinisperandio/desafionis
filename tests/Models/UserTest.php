<?php
require_once __DIR__.'/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use App\Model\User;

class UserTest extends TestCase {

  protected $faker;
  private $user;

  public function setUp(): void
    {
      $this->faker = Faker\Factory::create();
      $this->user = new User();
    }

  public function testCreateNewUser()
  {
    $this->assertContains(true, $this->user->create($this->faker->name));
  }

  public function testCreateNewUserUnsuccess()
  {
    $name = $this->faker->name;
    $this->assertContains(true, $this->user->create($name));
    $this->assertContains(false, $this->user->create($name));
  }

  public function testFindUserById()
  {
    list($actual, $status) = $this->user->create($this->faker->name);
    $this->assertSame(true, $status);
    
    list($register, $status) = $this->user->findUserByNis($actual->nis);
    $this->assertSame( $actual->user_id, $register->user_id);
  }

  public function testFindUserByIdUnsuccess()
  {
    $this->assertContains(
      false, 
      $this->user->findUserByNis($this->faker->regexify('[0-9]{11}'))
    );
  }

  public function testFindUserByNameSuccess()
  {
    list($actual, $status) = $this->user->create($this->faker->name);
    $this->assertSame(true, $status);
    
    list($register, $status) = $this->user->findUserByName($actual->name);
    $this->assertSame( $actual->user_id, $register->user_id);
  }

  public function testFindUserByNameUnsuccess()
  {
    $this->assertEmpty($this->user->findUserByName($this->faker->name));
  }
}