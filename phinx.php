<?php
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();

    return [
        "paths" => [
            "migrations" => "db/migrations",
            "seeds" => "db/seeds"
        ],
        "environments" => [
            "default_migration_table" => "phinxlog",
            "default_environment" => "development",
            "development" => [
                "adapter" => "mysql",
                "host" => $_ENV['DB_HOST'],
                "name" => $_ENV['DB_DATABASE'],
                "user" => $_ENV['DB_USERNAME'],
                "pass" => $_ENV['DB_PASSWORD'],
                "port" => $_ENV['DB_PORT']
            ]
        ]
    ];