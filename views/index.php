<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gesuas NIS</title>

  <link rel="stylesheet" href="/css/main.css" type="text/css">

</head>
<body>
  <div class="container">
    <div id="search">
      <h1 class="title">Bem vindo</h1>
      <p class="paragraphe">
        Quer procurar alguém pelo NIS?
        <br><br>
        Diga o NIS de quem tu procuras e eu direi quem és
      </p>
      <form action="?r=/find" method="POST">
        <div class="group-form">
          <input type="text" name="nis" id="nis" type="text" placeholder="NIS" required>
        </div>
        <div class="group-form">
          <input type="submit" class="btn search" value="Buscar">
        </div>
      </form>
    </div>
    <div id="subscription">
      <h1 class="title">Não possui um cadastro?</h1>
      <p class="paragraphe">
        É super rápido, basta inserir seu nome completo.
      </p>
      <form action="?r=/user" method="POST">
        <div class="group-form">
          <input type="text" name="name" id="name" placeholder="Nome" required>
        </div>
        <div class="group-form">
          <input type="submit" class="subscription" value="Cadastrar">
        </div>
      </form>
    </div>
  </div>
</body>
</html>