<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gesuas NIS</title>

  <link rel="stylesheet" href="/css/main.css" type="text/css">

</head>
<body>
  <div class="container">
    <div id="result">
      <h1 class="title"><?php echo $vars['titulo'] ?></h1>
      <p class="paragraphe">
        <?php echo $vars['text'] ?>
        <br><br>
        
        <?php if (!empty($vars['user'])): ?>
          <div class="view-result">
            <h1><?php echo "{$vars['user']['name']} - {$vars['user']['nis']}"?></h1>
            <img width="300" src="/images/great.png">
          </div>
        <?php else: ?>
          <img width="300" src="/images/confuse.png">
        <?php endif; ?>
      
      </p>
      <a href="?r=/" class="btn search">Home</a>
    </div>
  </div>
</body>
</html>