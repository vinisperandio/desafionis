<?php
require_once __DIR__.'/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '/../.env');
$dotenv->load();

use App\Router;
$app = new Router();

$app->get('/', function () {
    return \App\Controller\AppController::index();
});

$app->get('/list', function () {
    return \App\Controller\AppController::list();
});

$app->post('/find', function () {
    return \App\Controller\AppController::find();
});

$app->post('/user', function () {
    return \App\Controller\AppController::create();
});

$app->run();