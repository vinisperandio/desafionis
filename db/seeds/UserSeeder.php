<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 6; $i++) {
            $data[] = [
                'user_id' => $faker->uuid,
                'name' => $faker->name,
                'nis' => $faker->regexify('[0-9]{11}'),
            ];
        }

        $this->table('users')->insert($data)->saveData();
    }
}
