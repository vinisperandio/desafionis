<?php
namespace App\Model;

use Ramsey\Uuid\Uuid;
use App\Model\Connector;

class User {

    private $db;
    protected $conn;

    public function __construct()
    {        
        $this->db = new Connector();
        $this->conn = $this->db->getConnection();
    }

    /**
     * @param String $nis
     * @return array
     */
    public function findUserByNis($nis)
    {
        $user = array();
        $query = "SELECT *"
                . "FROM users "
                . "WHERE users.nis = '%s'";
        $query = sprintf($query, $this->conn->real_escape_string($nis));

        if ($result = $this->conn->query($query))
        {
            $row = $result->fetch_assoc();
            
            if (!$row) return array ('Usuário não encontrado', false);
            
            return array( array(
                'user_id' => $row['user_id'],
                'name' => $row['name'],
                'nis' => $row['nis']),
                true
            );
            $result->close();
        } else {
            return array("Error: " . $sql . "<br>" . $this->conn->error, false);
        }
    }

    public function listAll() {
        $user = array();
        $query = "select * from users";

        if ($result = $this->conn->query($query))
        {
            $data = mysqli_fetch_all( $result, MYSQLI_ASSOC );

            foreach ($data as $row) {
                array_push($user, array(
                    'user_id' => $row['user_id'],
                    'name' => $row['name'],
                    'nis' => $row['nis'])
                );
            }
            
            $result->close();
        } else
            die($this->con->error);
        return $user;
    }

    /**
     * @param String $nis
     * @return array
     */
    public function findUserByName($name)
    {
        $user = array();
        $query = "SELECT *"
                . "FROM users "
                . "WHERE users.name = '%s'";
        $query = sprintf($query, $this->conn->real_escape_string($name));

        if ($result = $this->conn->query($query))
        {
            $row = $result->fetch_assoc();
            
            if (!$row) return false;
            
            $user = array(
                'user_id' => $row['user_id'],
                'name' => $row['name'],
                'nis' => $row['nis']
            );
            $result->close();
        } else
            die($this->conn->error);
        return $user;
    }

    /**
     * @param String $name
     * @return array
     */
    public function create(string $name) 
    {
        if (empty($this->findUserByName($name))) {
            $uuid = Uuid::uuid4();
            $user = array();
            $nis = str_pad( mt_rand( 1, 99999999999 ), 11, '0', STR_PAD_LEFT );
            
            $query = "INSERT INTO users(user_id, name, nis)"
                    . "VALUES ('%s', '%s', '%s')";
            $query = sprintf($query, $this->conn->real_escape_string($uuid), $this->conn->real_escape_string($name), $this->conn->real_escape_string($nis));
            
            if ($this->conn->query($query) === TRUE) {
                return array( array(
                    'user_id' => $uuid,
                    'name' => $name,
                    'nis' => $nis), 
                    true
                );
            } else {
                return array("Error: " . $sql . "<br>" . $this->conn->error, false);
            }
            
        } else {
            return array('Usuário já cadastrado!', false);
        }
    }
}