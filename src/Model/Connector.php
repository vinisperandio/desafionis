<?php

namespace App\Model;

class Connector {

    public $conn;
    private $config;

    function __construct() {
        include(__DIR__."/../../config/config.php");
        $this->config = $config;
    }

    public function getConnection()
    {
        $conn = new \mysqli($this->config['DB_HOST'], $this->config['DB_USERNAME'], $this->config['DB_PASSWORD'], $this->config['DB_DATABASE']);
        // Check connection
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        return $conn;
    }

    public function closeConnection()
    {
        mysqli_close($con);
    }
}