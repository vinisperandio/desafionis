<?php
namespace App\Controller;

use App\Model\User;

final class AppController extends Controller {

    public static function index() {
        return self::view('index');
    }

    public static function list() {
        $users = (new User)->listAll();
        return self::view('list', ['users' => $users]);
    }

    public static function find() {
        if ( (self::params('nis') ==! '') && (strlen(self::params('nis')) == 11) ) {
            list($data, $status) = (new User)->findUserByNis( trim(self::params('nis')) );

            if ($status) {
                return self::view('result', [
                    'titulo' => 'Quem procura acha!',
                    'text' => 'Encontramos a sua pessoa',
                    'user' => $data
                    ]
                );
            } else {
                return self::view('result', [
                    'titulo' => 'Ops!',
                    'text' => $data
                    ]
                );
            }
            
        } else {
            return self::view('result', ['titulo' => 'NIS no formatado errado']);
        }
    }

    public static function create() {
        if ( self::params('name') ==! '' ) {
            list($data, $status)  = (new User)->create( trim(self::params('name')) );
            
            if ($status) {
                return self::view('result', [
                    'titulo' => 'NIS criado com sucesso!',
                    'text' => 'Dados cadastrados',
                    'user' => $data
                    ]
                );
            } else {
                return self::view('result', [
                    'titulo' => 'Ops!',
                    'text' => $data
                    ]
                );
            }
            
        } else {
            return self::view('result', ['titulo' => 'Nome do usuário obrigatório']);
        }
    }
}