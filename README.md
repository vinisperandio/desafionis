## Tecnologias
- PHP
- HTML
- CSS

## Sobre
Uma aplicação de cadastro e geração do NIS. Contém um formulário para cadastrar e outro para pesquisa de cidadãos.
Além de uma tela de resultado.

Todos os critérios (obrigatórios e extras) foram cumpridos.

Como um dos critérios era a utilização de pacotes de terceiros, eu optei por utilizar apenas pacotes que auxiliam na criação do banco de dados,
pra facilitar o meu desenvolvimento e a avaliação da aplicação pelos avaliadores.

O front é responsivo e utliza apenas HTML e CSS

Para auxiliar na validação do código, a rota abaixo lista os usuários e seus respectivos NIS, para não precisar abrir uma IDE de banco. Essa rota precisa ser colocada manualmente no navegador. 
http://localhost/?r=/list

## Rodando o projeto 
> Todas as configurações (Database, Migrate e Docker) partem do arquivo ./env
- Database config: `./config/config.php`
- Migrate config: `./phinx.php`
- Docker config: `./docker-compose`
- Rodar testes: `composer run test`

```shell script
   composer install
   php vendor/bin/phinx migrate -e development
   php vendor/bin/phinx seed:run
   composer run test
```

## Docker
> Configure DOCKER_UID no .env   
- Alterar configurações em `.env`
- Altere o host para conexão com o banco pelo nome dos serviços `db`
- Montar as imagens `docker-compose build`
- Levantar os serviços `docker-compose up -d`
- Instalar dependencias PHP `docker-compose exec app composer install`
- Aplicar migrations `docker-compose exec app vendor/bin/phinx migrate -e development`
- Aplicar seed `docker-compose exec app vendor/bin/phinx seed:run`
- Rodar Testes `docker-compose exec app composer run test`
